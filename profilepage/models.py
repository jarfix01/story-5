from django.db import models
from django import forms

# Create your models here.

class Friend(models.Model):
    name = models.CharField(max_length= 20)
    hobby = models.CharField(max_length = 200)
    favorite_food = models.CharField(max_length = 200)
    class_year = models.CharField(max_length = 200)

    def __str__ (self):
        return self.name

class ClassYear(models.Model):
    class_year = models.ForeignKey(Friend, on_delete=models.CASCADE)


