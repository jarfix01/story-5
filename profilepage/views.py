from django.shortcuts import render, redirect
from .forms import FriendForm
from .models import Friend
from django.contrib import messages
# Create your views here.

def home (request):
    return render(request, 'profilepage/Home.html')

def about (request):
    return render(request, 'profilepage/About.html')

def contact (request):
    return render(request, 'profilepage/Contact.html')

def resume (request):
    return render(request, 'profilepage/Resume.html')

def friends(request):
    context = {
        'friends' : Friend.objects.all()
        }
    return render (request, 'profilepage/Friend_page.html', context)

def add(request):
    if request.method == 'POST':
        form = FriendForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, f'New Friend Added')
            return redirect('add')

    else:
        form = FriendForm()
        return render (request, 'profilepage/Add_Friend.html',{'form' : form})
